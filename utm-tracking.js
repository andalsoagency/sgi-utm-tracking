<script>
    function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.search);
    if(results == null) {
        return "";
    } else {
        return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}

$(document).ready(function () {
    function addFormElem(paramName, fieldName) {
        var paramValue = getParameterByName(paramName);
        var $utmEl = $("<input type='hidden' name='" + fieldName + "' value='" + paramValue + "'>");
        if (paramValue != "") {
            $('#lead_source_most_recent_detail').after($utmEl);
        }
    }

    // Note: Only change the values after ":"
    var utmParams = {
        "utm_source"   : "utm_source",
        "utm_medium"   : "utm_medium",
        "utm_campaign" : "utm_campaign",
        "utm_content"  : "utm_content",
        "utm_term"     : "utm_term",
        "ppc_adgroup"  : "ppc_adgroup",
        "ppc_loc"      : "ppc_loc"
    };

    for (var param in utmParams) {
        addFormElem(param, utmParams[param]);
    }
}); 

</script>